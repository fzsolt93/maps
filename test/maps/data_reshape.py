import pandas as pd
import numpy as np
import json
import pycountry

df = pd.read_csv('population.csv')

# for i in range(1950,2100):


pop = {}
ydict = {}
for i in range(1950,2101):
	data = []
	cdict = {}
	data = df.loc[df['Time'] == i, ['Location','Time','PopTotal']]	
	for index,row in data.iterrows():

		user_input = row['Location']
		mapping = {country.name: country.alpha_3 for country in pycountry.countries}
		cdict[mapping.get(user_input)] = row['PopTotal']

	ydict[str(i)] = cdict

pop["Storm"] = ydict

import json
with open('data_all.json', 'w') as outfile:
    json.dump(pop, outfile)
